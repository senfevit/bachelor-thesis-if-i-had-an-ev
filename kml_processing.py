from datetime import datetime as dt
from datetime import timedelta as tdel
import xmltodict
import json

def pointStringToDict(str):
    '''
    convert point from string to dict
    :param str: str of point
    :return: {'longtitude':float, 'latitude':float}
    '''
    list = str.split(',')
    ret = {}
    ret["longitude"] = float(list[0])
    ret["latitude"] = float(list[1])
    return ret

def getStartingTime(dct):
    '''
    get starting time of activities sequence  from dict
    :param dct: dict with times of starting action
    :return: string with formated datetime
    '''
    strt = dct['end']
    date_obj = dt.strptime(strt, '%Y-%m-%dT%H:%M:%S.%fZ')
    #print(date_obj)
    return date_obj.strftime("%Y-%m-%dT%H:%M:%S+00:00")

def getEndingTime(dct): ##konecna pozice nema nastaveny cas
    '''
    get ending time of activities sequnce
    :param dct: dict with times of last action
    :return: string with formated datetime
    '''
    strt = dct['begin']
    date_obj = dt.strptime(strt, '%Y-%m-%dT%H:%M:%S.%fZ')
    #print(date_obj)
    return date_obj.strftime("%Y-%m-%dT%H:%M:%S+00:00")
def getTimeInterval(dct, max_delays):
    '''
    get starting and ending time + duration of one activity
    :param dct: times of activity
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: {'from':str, 'end':str}, int
    '''
    ret = {}
    strt = dct['end']
    date_objT = dt.strptime(strt, '%Y-%m-%dT%H:%M:%S.%fZ')
    strt = dct['begin']
    date_objF = dt.strptime(strt, '%Y-%m-%dT%H:%M:%S.%fZ')
    dur = int(tdel.total_seconds(date_objT - date_objF))
    date_objT = date_objT + tdel(minutes=max_delays['departure_delay'])
    ret['to'] = date_objT.strftime("%Y-%m-%dT%H:%M:%S+00:00")
    date_objF = date_objF + tdel(minutes=max_delays['arrival_delay'])
    ret['from'] = date_objF.strftime("%Y-%m-%dT%H:%M:%S+00:00")
    return [ret], dur

def getGoalPos(str):
    '''
    returns position dict
    :param str: position string
    :return: {'location':{}}
    '''
    dct = {}
    dct['location'] = pointStringToDict(str)
    return [dct]

def getGoal(act, id, max_delays):
    '''
    create one goal
    :param act: orderedDict with activity
    :param id: id of activity
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: {'duration':int, 'goalId':int, 'elegiblePois':{}, intervals:{}}
    '''
    ret = {}
    intervals, duration =getTimeInterval(act['TimeSpan'], max_delays)
    ret['duration'] = duration
    ret['goalID'] = id
    ret['eligiblePois'] = getGoalPos(act['Point']['coordinates'])
    ret['intervals'] = intervals
    return ret

def getAllGoals(listOfGoals, max_delays):
    '''
    create list of all goals or empty list
    :param listOfGoals: string with goals
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: []
    '''
    if listOfGoals == []:
        return []
    else:
        ret = []
        for i in range(0, len(listOfGoals)):
            ret.append(getGoal(listOfGoals[i], i+1, max_delays)) #goals jsou o jedno posunute, protoze nula = startpoint
        return ret

def kmlToLst(str):
    '''
    parse google kml into list suitable for using in convertion to json
    :param str: string of loaded kml file
    :return: []
    '''
    kmldict = xmltodict.parse(str)
    for a in kmldict.values():
        kmldict = a
    for a in kmldict.values():
        kmldict = a
    for a in kmldict.values():
        kmldict = a
    lst = []
    for a in kmldict:
        #print(a)
        lst.append(a)
    return lst

def filterKMLlist(lst):
    '''
    filtering kml list -> remove all transports + activities that were not reached/ leaved by car
    :param lst: [] (output of kmlToLst)
    :return: []
    '''
    filteredlst = []  # list of dictionaries with activities??
    # nejsnazsi je jen kontrolovat jestli mezi aktivitama byla jizda a zbytek udaju brat z aktivit
    # zpracovavat jizdu se zda byt zbytecne slozite byt by se mohlo hodit pokud parkuje jinde nez kona aktivutu nebo je dalsi/predchozi aktivita chuze..
    names = [] # list kde index = goalId (0 = start, posledni = konec)
    w = 0 # byla aktivita chození?
    for i, smt in enumerate(lst):
        if smt['name'] == 'Walking':
            w = 1
            print('walking')
        elif smt['name'] == 'Driving':
            w = 0
            print('driving')
        elif smt['name'] == 'Moving':
            w = 0
            print('driving')
        elif not w:
            filteredlst.append(smt)
            names.append(smt['name'])
        else:
            if i < len(smt)-1:
                if lst[i+1]['name'] == 'Driving':
                    filteredlst.append(smt)
                    names.append(smt['name'])
                else:
                    print('filtered')
            else:
                print('filtered')
    return filteredlst, names

def createNewJSONdict(filteredlst, max_delays, vehicle):
    '''
    create json from !filtered! list
    :param filteredlst: []
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: {} suitable for convertion to json
    '''
    lastPoint = filteredlst[-1]
    filteredlst.pop(-1)
    firstPoint = filteredlst[0]
    filteredlst.pop(0)
    jsondict = {}
    jsondict['id'] = 1
    jsondict['vehicle']= vehicle
    jsondict['startTime'] = getStartingTime(firstPoint['TimeSpan'])
    jsondict['userPosition'] = pointStringToDict(firstPoint['Point']['coordinates'])
    jsondict['goals'] = getAllGoals(filteredlst, max_delays)
    jsondict['preferences'] = {"trip-preferences": ["FASTESTTRIP"]}# zbytecne nechat to pocit tolik"GREENESTTRIP", "FASTESTTRIP", "MINIMUMCHARGINGCOST"]}  ##nevim co presne dela tohle, doresit
    jsondict['endPosition'] = pointStringToDict(lastPoint['Point']['coordinates'])
    return jsondict

def saveJSON(jsondict, name):
    '''
    saving dict as json json
    :param jsondict: {}
    :param name: str
    :return: --
    '''
    with open(name, 'w') as fp:
        json.dump(jsondict, fp)

def loadKMLfile(name):
    '''
    loading kml file
    :param name: str
    :return: str
    '''
    return open(name, 'r').read()

def kmlToJSONfile(input_names, output_name,vehicle, max_delays = None):
    '''
    convert input kml into json that could be used with planner
    :param input_names: [str] - names of input google kml
    :param output_name: str - name of output json
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: [] - list of names of activities, [] - list of all activities - for comparison
    '''
    if max_delays == None:
        max_delays = {'arrival_delay':0, 'departure_delay':0}
    all_act = []
    all_act_names = []
    for name in input_names:
        k = loadKMLfile(name)
        l = kmlToLst(k)
        f, n = filterKMLlist(l)
        all_act += f
        all_act_names += n
    j = createNewJSONdict(all_act.copy(), max_delays, vehicle)
    saveJSON(j, output_name)
    print('saved!')
    return all_act_names, all_act

def getActivityForComparison(act, id):
    '''
    create one goal
    :param act: orderedDict with activity
    :param id: int
    :return: {'duration':int, 'goalId':int, 'Pos':{}, 'intervals':{}, 'name':str}
    '''
    ret = {}
    max_delays = {'arrival_delay': 0, 'departure_delay': 0}
    intervals, _ =getTimeInterval(act['TimeSpan'], max_delays)
    ret['Pos'] = getGoalPos(act['Point']['coordinates'])
    ret['goalID'] = id
    ret['intervals'] = intervals
    ret['name'] = act['name']
    return ret

def activitiesKMLlistToactivitesTimesList(all_act):
    '''
    converts list of all activites to new list suitable for later use while processing results
    :param all_act: second output of kmlToJSON
    :return: []
    '''
    new_all_act = []
    id = 0
    for act in all_act:
        new_all_act.append(getActivityForComparison(act,id))
        id += 1
    return new_all_act

def kmlToJSON(onekml, vehicle,max_delays = None):
    '''
    convert input kml into json that could be used with planner
    :param input_names: [str] - names of input google kml
    :param output_name: str - name of output json
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: [] - list of names of activities, [] - list of all activities - for comparison
    '''
    if max_delays == None:
        max_delays = {'arrival_delay':0, 'departure_delay':0}
    all_act = []
    all_act_names = []
    l = kmlToLst(onekml)
    f, n = filterKMLlist(l)
    all_act += f
    all_act_names += n
    j = createNewJSONdict(all_act.copy(), max_delays, vehicle)
    print('saved!')
    return all_act_names, all_act, j