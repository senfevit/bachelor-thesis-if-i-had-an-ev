import json
import datetime
import pandas as pd
import geopandas as gpd
import response_processing as rp

def getDrivesLengths(actions):
    nondrivingactions, total_duration, drives = rp.filterNonDrivingActions(actions)
    linestrings = rp.getLineString(drives)
    # print(linestrings.length)
    # print(linestrings.crs)
    # print(sum(linestrings.length))
    return linestrings.length, sum(linestrings.length)

def getSumKWH(actions):
    nondrivingactions, total_duration, drives = rp.filterNonDrivingActions(actions)
    sum_kwh = 0
    for action in drives:
        sum_kwh += float(action['planAction']['consumedEnergy'])/1000000
    return sum_kwh

def getTotalTime(actions): # brat to primo z duration je nevhodne, protoze my do toho nechceme zapocitavat pauzy (z planu)
    # zatimco duration property u planu je ciste cas prijezdu do konce - cas odjezdu ze zacatku...
    total_time = 0
    # for action in actions:
    #     total_time += action['duration']
    ln = len(actions)
    for i in range(0, ln):
        if i < ln - 1:
            if actions[i]['planAction']['type'] != 'DRIVE':
                if actions[i+1]['planAction']['type'] != 'DRIVE':
                    total_time += max(actions[i]['duration'], actions[i+1]['duration'])
                    i += 1
                else:
                    total_time += actions[i]['duration']
            else:
                total_time += actions[i]['duration']
        else:
            total_time += actions[i]['duration']
    total_time = total_time
    return total_time

def getDelaysAndDurationsList(actions, icev_actions,original):
    filteredICEV = filterICEV(icev_actions)
    delays = []
    durations = []
    delays.append(0)
    durations.append(0)  #prvni originalni aktivitu netreba resit - je to start a tam je delay i duration nula
    dur = 0
    orig = 1
    icev = 0
    for i in range(0, len(actions)): #zaciname od nuly jelikoz prvni je cesta, z posledniho bereme jen dur
        pom = actions[i]
        if pom['planAction']['type'] != 'DO_ACTIVITY':
            dur += pom['duration']
        else:
            durations.append(dur)
            dur = 0
            pomdel = (calcDelay(pom, filteredICEV[icev], original[orig]))
            delays.append(pomdel)
            orig += 1
            icev += 1
    last_del = getLastActionsTimeDif(actions[-1], icev_actions[-1],original[-1])
    delays.append(last_del)
    durations.append(dur)
    return delays, durations

def filterICEV(actions):
    filtered = []
    for a in actions:
        if a['planAction']['type'] == 'DO_ACTIVITY':
            filtered.append(a)
    return filtered

def calcDelay(bev, icev, orig):
    bev_time = tryToGetTime(bev)
    icev_time = tryToGetTime(icev)
    orig_time = datetime.datetime.strptime(orig['intervals'][0]['from'], "%Y-%m-%dT%H:%M:%S+00:00")
    delayICEV = (bev_time - icev_time).total_seconds()
    if (delayICEV <= 0):
        return 0
    delayOrig = (bev_time - orig_time).total_seconds()
    if (delayOrig <= 0):
        return 0
    return (min(delayOrig, delayICEV))

def tryToGetTime(act):
    try:
        new_time = datetime.datetime.strptime(act['from'], '%Y-%m-%dT%H:%M:%SZ')  # zkusi klasicky format
    except:
        try:
            new_time = datetime.datetime.strptime(act['from'],'%Y-%m-%dT%H:%MZ')
        except:
            try:
                new_time = datetime.datetime.strptime(act['from'],'%Y-%m-%dT%HZ')
            except:
                new_time = datetime.datetime.strptime('0000-1-1T00:00', '%Y-%m-%dT%H:%MZ')
    return new_time

def calcDelayComparison(actions, orig):
    plan_time = tryToGetTime(actions)
    orig_time = datetime.datetime.strptime(orig['intervals'][0]['from'], "%Y-%m-%dT%H:%M:%S+00:00")
    delayOrig = (plan_time - orig_time).total_seconds()
    return delayOrig

def getDelayComparison(actions, original):
    delays = []
    delays.append(0)
    orig = 1
    for i in range(0, len(actions)): #zaciname od nuly jelikoz prvni je cesta, z posledniho bereme jen dur
        pom = actions[i]
        if pom['planAction']['type'] == 'DO_ACTIVITY':
            pomdel = (calcDelayComparison(pom, original[orig]))
            delays.append(pomdel)
            orig += 1
    last_del = getLastActionsTimeDifComparison(actions[-1], original[-1])
    delays.append(last_del)
    return sum(delays)

def getLastActionsTimeDif(last_drive, icev_drive, orig_last_activity):
    new_time = tryToGetTime(last_drive)
    icev_time = tryToGetTime(icev_drive)
    old_time = datetime.datetime.strptime(orig_last_activity['intervals'][0]['from'], "%Y-%m-%dT%H:%M:%S+00:00")
    new_time = new_time + datetime.timedelta(seconds=last_drive['duration'])
    icev_time = icev_time + datetime.timedelta(seconds=icev_drive['duration'])
    if new_time <= old_time:
        return 0
    elif new_time <= icev_time:
        return 0
    else:
        total_delay = min((new_time - old_time).total_seconds(),(new_time-icev_time).total_seconds())
    return total_delay

def getLastActionsTimeDifComparison(last_drive, orig_last_activity):
    new_time = tryToGetTime(last_drive)
    old_time = datetime.datetime.strptime(orig_last_activity['intervals'][0]['from'], "%Y-%m-%dT%H:%M:%S+00:00")
    new_time = new_time + datetime.timedelta(seconds=last_drive['duration'])
    if new_time <= old_time:
        return 0
    total_delay = (new_time - old_time).total_seconds()
    return total_delay

def getPriceOfDrivesEV(actions, kwh_price):
    sum_kwh = getSumKWH(actions)
    return kwh_price * sum_kwh

def getPriceOfDrivesICEV(actions, fuel_cost, per100km_average_consumption):
    _, len_metres = getDrivesLengths(actions)
    len100km = len_metres/(1000 * 100) # z metru na kilometry a ty pak na normu spotreby -> 100 km
    consumption = len100km *per100km_average_consumption
    return fuel_cost * consumption

def getChargingTime(actions):
    _, charging_actions = rp.filterNonChargingActions(actions)
    _, duration = rp.getPriceAndDurationOfCharging(charging_actions)
    return duration

def calculateTotalPollutionICEV(actions, pol_const, per100km_average_consumption):
    _, len_metres = getDrivesLengths(actions)
    len_km = len_metres / 1000 # z metru na kilometry
    len_100km = float(len_km) / 100
    fuel_cons = len_100km * per100km_average_consumption
    pol = pol_const * fuel_cons
    return pol

def calculateTotalPollutionBEV(actions, pol_const):
    kwh = getSumKWH(actions)
    pol = pol_const * kwh
    return pol

def getTotalKWHcharged(actions):
    _, charging_actions = rp.filterNonChargingActions(actions)
    charged = 0
    for a in charging_actions:
        charged += a['planAction']['chargedEnergy']
    charged = float(charged)
    charged /= 1000000 #z mili na kilo
    return charged

def timeScore(ev_actions, icev_actions, time_score_value):
    icev_time = getTotalTime(icev_actions)
    ev_time = getTotalTime(ev_actions)
    time_score = (ev_time - icev_time) /icev_time
    time_score = time_score_value - time_score_value * time_score
    return time_score

def delayScore(ev_actions, icev_actions ,original, delay_score_value):
    delays, durations = getDelaysAndDurationsList(ev_actions,icev_actions, original)
    pom = 0.0
    for i in range(1, len(delays)): #protoze startovaci pozice je vzdy zpozdeni nula, ze jo
        pom += (delays[i]**2) / (4*durations[i])
    pom /= (len(delays) - 1) #prvni aktivita(pozice) nema zpozdeni
    delay_score = delay_score_value - delay_score_value * pom
    delay_score = max(delay_score, (-2/3)*delay_score_value)
    return delay_score

def priceScore(ev_actions, icev_actions, price_score_value, kwh_price, fuel_cost, per100km_average_consumption):
    ev_price = getPriceOfDrivesEV(ev_actions, kwh_price)
    icev_price = getPriceOfDrivesICEV(icev_actions, fuel_cost, per100km_average_consumption)
    price_score = ev_price / icev_price
    price_score = price_score_value - price_score_value * price_score
    return price_score

def greenScore(ev_actions, icev_actions, green_score_value, ev_pol, icev_pol, per100km_fuel):
    icev_pol = calculateTotalPollutionICEV(icev_actions, icev_pol, per100km_fuel)
    ev_pol = calculateTotalPollutionBEV(ev_actions, ev_pol)
    green_score =  ev_pol / icev_pol
    green_score = green_score_value - green_score_value * green_score
    return green_score

def calculateTotalScore(time, delay, price, green):
    total = time + delay + price + green
    total = min(total, 100)
    total = max(total, 0)
    return total

def getScore(ev_actions, icev_actions, values, ev_pol, icev_pol, per100km_fuel, original,  kwh_price, liter_fuel_price):
    suma = 0
    for k in values.keys():
        suma += values[k]
    if suma != 100:
        print('sum of scores != 100, scores are reweighted')
        for k in values.keys():
            values[k] = (values[k]/suma) * 100
    time_score = timeScore(ev_actions, icev_actions, values['time'])
    delay_score = delayScore(ev_actions, icev_actions, original, values['delay'])
    price_score = priceScore(ev_actions, icev_actions, values['price'], kwh_price, liter_fuel_price, per100km_fuel)
    green_score = greenScore(ev_actions, icev_actions, values['green'], ev_pol, icev_pol, per100km_fuel)
    electrification_score = calculateTotalScore(time_score, delay_score, price_score, green_score)
    return electrification_score, {'time':time_score, 'delay':delay_score, 'price':price_score, 'green':green_score}

def createComparison(ev_actions, icev_actions, original, values = None, params = None):
    if values == None:
        values = {'time': 45, 'delay': 30, 'price': 15, 'green': 10}
    if params == None:
        params = {'kwh_price': 3, 'liter_fuel_price': 30, 'per100km_fuel': 9, 'icev_pol_per_liter': 2310, 'bev_pol_per_kwh': 780, 'currency': 'Kč'}
    # jak strukturovat comparison file? idealni asi json kde bev
    comparison ={
        'Total_time':{ #sekundy
            'bev': 0,
            'icev': 0
        },
        'Delay':{  #sekundy
            'bev': 0,
            'icev': 0
        },
        'Km_driven':{
            'bev': 0,
            'icev': 0
        },
        'Cost':{
            'bev': 0,
            'icev': 0,
            'currency': 'Kč'
        },
        'Saved_CO2':0,
        'Charging_time': 0,
        'Charged_kwh': 0,
        'Spent_kwh': 0,
        'Electrification_score': 0,
        'Score_explanation': 'Something'
    }
    comparison['Delay']['bev'] = getDelayComparison(ev_actions, original)
    comparison['Delay']['icev'] = getDelayComparison(icev_actions, original)
    _, bev_km = getDrivesLengths(ev_actions)
    _, icev_km = getDrivesLengths(icev_actions)
    comparison['Km_driven']['bev'] = float(bev_km) / 1000
    comparison['Km_driven']['icev'] = float(icev_km) / 1000
    comparison['Total_time']['bev'] = getTotalTime(ev_actions)
    comparison['Total_time']['icev'] = getTotalTime(icev_actions)
    comparison['Cost']['bev'] = getPriceOfDrivesEV(ev_actions, params['kwh_price'])
    comparison['Cost']['icev'] = getPriceOfDrivesICEV(icev_actions, params['liter_fuel_price'], params['per100km_fuel'])
    comparison['Cost']['currency'] = params['currency']
    comparison['Charging_time'] = getChargingTime(ev_actions)
    comparison['Charged_kwh'] = getTotalKWHcharged(ev_actions)
    comparison['Spent_kwh'] = getSumKWH(ev_actions)
    comparison['Saved_CO2'] = calculateTotalPollutionICEV(icev_actions, params['icev_pol_per_liter'], params['per100km_fuel']) - calculateTotalPollutionBEV(ev_actions, params['bev_pol_per_kwh'])
    score, subscores = getScore(ev_actions, icev_actions,values, params['bev_pol_per_kwh'],params['icev_pol_per_liter'], params['per100km_fuel'], original, params['kwh_price'], params['liter_fuel_price'])
    comparison['Electrification_score'] = score
    comparison['Score_explanation'] = getScoreExplanation(subscores, values, params)
    return comparison

def getScoreExplanation(subscores, values, params):
    exp = ''
    if subscores['time'] < (2/3*values['time']):
        exp += 'Total time of BEV is too high compared to ICE vehicle \n'
    else:
        exp += 'Total time of BEV is OK \n'
    if subscores['delay'] < (2/3*values['delay']):
        exp += 'Activity arrival delays of BEV are too high compared to ICE vehicle \n'
    else:
        exp += 'Activity arrival delays of BEV are OK \n'
    if subscores['price'] < (2/3*values['price']):
        exp += '(Fuel price) Savings from adapting BEV are not high enough \n'
    else:
        exp += '(Fuel price) Savings of BEV are OK \n'
    if subscores['green'] < ((params['bev_pol_per_kwh']/params['icev_pol_per_liter'])*values['green']):
        exp += 'Total CO2 pollution created by BEV is similar to ICE vehicle pollution (in this situation) \n'
    else:
        exp += 'Great pollution reduction \n'
    return exp



