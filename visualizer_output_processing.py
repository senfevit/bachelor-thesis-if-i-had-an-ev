import json
from datetime import datetime as dt
from datetime import timedelta as tdel

def geojsonFileToJSON(visualizer_output_name, max_delays = None, vehicle = None):
    '''
    Converts visualizer output to json accepted by planner on input
    :param visualizer_output_name: str - name of file with visualizer output
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: {} - with every information and formated in such way it is accepted by planner
    '''
    if max_delays == None:
        max_delays = {'arrival_delay':0, 'departure_delay':0}
    visualizer_geojson = open(visualizer_output_name, 'r').read()
    visualizer_dict = json.loads(visualizer_geojson)
    #print(visualizer_dict)
    visualizer_list = visualizer_dict['features']
    print("PROCESSING ...")
    #print(visualizer_dict)
    jsondict = addStart(visualizer_list[0], {})
    goal_list = []
    if len(visualizer_list) > 2:
        for i in range(1, len(visualizer_list)-1): #posledni se procesuje zvlast!
            goal_list.append(createGoal(visualizer_list[i], i, max_delays))
    jsondict['goals'] = goal_list
    if vehicle == None:
        jsondict['vehicle'] = setVehicle(visualizer_list[0]['properties'])
    else:
        jsondict['vehicle'] = vehicle
    jsondict = addEnd(visualizer_list[-1], jsondict)
    return jsondict

def addStart(goal, jsondict):
    '''
    Adds new keys to dictionary corresponding to starting position
    :param goal: {} - activity - first activity from visualizer input
    :param jsondict:
    :return:
    '''
    jsondict['id'] = 1
    jsondict['startTime'] = getStartingTime(goal['properties']['datetimes'])
    jsondict['userPosition'] = convertVisualizerPointToPlannerPoint(goal)
    return jsondict

def createGoal(goal, id, max_delays):
    '''
    create one goal
    :param goal: orderedDict with activity
    :param id: id of activity
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: {'duration':int, 'goalId':int, 'elegiblePois':{}, intervals:{}}
    '''
    ret = {}
    intervals, duration = getTimeInterval(goal['properties']['datetimes'], max_delays, goal['properties']['duration'])
    ret['duration'] = int(goal['properties']['duration'])*60
    ret['goalID'] = id
    ret['eligiblePois'] = [{'location':convertVisualizerPointToPlannerPoint(goal)}]
    ret['intervals'] = intervals
    return ret

def addEnd(goal, jsondict):
    '''
    Adds part to json that are connected with end of activity sequence
    :param goal: {} - activity - last activity from visualizer input
    :param jsondict: {} - dictionary that will be used for creating planner input
    :return: {} - updated jsondict
    '''
    jsondict['preferences'] = {"trip-preferences": ["FASTESTTRIP"]}# zbytecne nechat to pocit tolik"GREENESTTRIP", "FASTESTTRIP", "MINIMUMCHARGINGCOST"]}  ##nevim co presne dela tohle, doresit
    jsondict['endPosition'] = convertVisualizerPointToPlannerPoint(goal)
    return jsondict

def getStartingTime(times):
    '''
    get starting time of activities sequence  from dict
    :param times: list with times of starting action
    :return: string with formated datetime
    '''
    strt = times
    date_obj = dt.strptime(strt, '%Y-%m-%dT%H:%M:%S')
    #print(date_obj)
    return date_obj.strftime("%Y-%m-%dT%H:%M:%S+00:00")

def convertVisualizerPointToPlannerPoint(goal):
    '''
    Converts visualizer output geometry to geometry accepted by planner
    :param goal: [float, float] = [longitude, latitude]
    :return: {'longitude':float, 'latitude':float}
    '''
    geometry = {}
    geometry['longitude'] = goal['geometry']['coordinates'][0]
    geometry['latitude'] = goal['geometry']['coordinates'][1]
    if goal['properties']['cs'] == True:
        geometry['charging_station'] = {
              "cluster_id": 1,
              "lat": goal['geometry']['coordinates'][1],
              "lon": goal['geometry']['coordinates'][0],
              "timesteps_per_timeslot": 1,
              "ntimeslots": 1,
              "policy": [
                [
                  [
                    18.333334
                  ],
                  [
                    18.333334
                  ],
                  [
                    18.333334
                  ]
                ]
              ],
              "capacity": 1,
              "user_model_const_price": 0,
              "user_model_price_slope": 0.0,
              "price_range": None,
              "absolute_edge_demand": None,
              "charging_rate": 3.055555582046509
            }
    return geometry

def getTimeInterval(times, max_delays, duration):
    '''
    get starting and ending time + duration of one activity
    :param times: [] - times of activity [starting, ending]
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: {'from':str, 'end':str}, int
    '''
    ret = {}
    strt = times
    # print(times)
    # print(max_delays)
    date_objF = dt.strptime(strt, '%Y-%m-%dT%H:%M:%S')
    date_objT = date_objF + tdel(minutes=int(duration))
    date_objT = date_objT + tdel(minutes=max_delays['departure_delay'])
    ret['to'] = date_objT.strftime("%Y-%m-%dT%H:%M:%S+00:00")
    date_objF = date_objF + tdel(minutes=max_delays['arrival_delay'])
    ret['from'] = date_objF.strftime("%Y-%m-%dT%H:%M:%S+00:00")
    return [ret], int(tdel.total_seconds(date_objT - date_objF))

def geojsonToJSON(visualizer_dict, max_delays = None, vehicle = None):
    '''
    Converts visualizer output to json accepted by planner on input
    :param visualizer_output_name: str - name of file with visualizer output
    :param max_delays: {'arrival_delay':int, 'departure_delay':int} - set delays in !minutes!
    :return: {} - with every information and formated in such way it is accepted by planner
    '''
    if max_delays == None:
        max_delays = {'arrival_delay':0, 'departure_delay':0}
    #print(visualizer_dict)
    visualizer_list = visualizer_dict['features']
    print("PROCESSING ...")
    #print(visualizer_dict)
    jsondict = addStart(visualizer_list[0], {})
    goal_list = []
    if len(visualizer_list) > 2:
        for i in range(1, len(visualizer_list)-1): #posledni se procesuje zvlast!
            goal_list.append(createGoal(visualizer_list[i], i, max_delays))
    jsondict['goals'] = goal_list
    if vehicle == None:
        jsondict['vehicle'] = setVehicle(visualizer_list[0]['properties'])
    else:
        jsondict['vehicle'] = vehicle
    jsondict = addEnd(visualizer_list[-1], jsondict)
    return jsondict

def extractInfo(visualizerDict):
    names = []
    activities = []
    i = 0
    for a in visualizerDict['features']:
        names.append(a['properties']['activity_name'])
        activities.append(convertOneActToInfoStyle(a,i))
        i+=1
    return names, activities

def convertOneActToInfoStyle(activity, id):
    ret = {}
    ret['Pos'] = [{'location':{'longitude':activity['geometry']['coordinates'][0], 'latitude':activity['geometry']['coordinates'][1]}}]
    ret['goalID'] = id
    date_objF = dt.strptime(activity['properties']['datetimes'], '%Y-%m-%dT%H:%M:%S')
    date_objT = date_objF + tdel(minutes=int(activity['properties']['duration'])) #nestabilni vystup z vizualiatoru - obcas vraci v aktualni verzi string
    ret['intervals'] = [{'from': date_objF.strftime("%Y-%m-%dT%H:%M:%S+00:00"), 'to':date_objT.strftime("%Y-%m-%dT%H:%M:%S+00:00")}]
    ret['name'] = activity['properties']['activity_name']
    return ret

def setVehicle(firstAction):
    if firstAction['battery_range'] == "":
        return {'range':500, 'battery_capacity':55}
    return {'range':firstAction['battery_range'], 'battery_capacity':firstAction['battery_capacity']}