import json
from datetime import datetime as dt
from datetime import timedelta as tdel
import pandas as pd
import geopandas as gpd
from shapely import wkt
from shapely.geometry import LineString

def getGeometryDrive(drive):
    '''
    creates geometry accaptable by visualizer from geometry returned by planner
    :param drive: {} - action drive returned from planner
    :return: {} - geometry specified for visualizer
    '''
    geometry = {}
    geometry['type'] = "LineString"
    df = pd.DataFrame(drive['planAction']['locations'])
    df = df.drop(['datetime'],axis=1)
    columns_titles = ["longitude", "latitude"]
    df = df.reindex(columns=columns_titles)
    df['pom'] = 450
    geometry['coordinates'] = df.values.tolist()
    return geometry

def getDatetimesAndTimestamps(start, end, num, locations):
    '''
    creates datetimes and datetimes_timestamps for properties of moving action for visualizer input
    :param start: datetime: starting time
    :param end: datetime: end time
    :param num: int - length of timestamp list
    :param locations: [] - as specified for planner output (part of dictionary with one action)
    :return: [datetimes], [timestamps]
    '''
    datetimes_ranges = []
    actual_num = 0
    locations = locations[1:-1]
    act = start
    sdf = 0
    for point in locations:
        sdf+=1
        if point['datetime'] != None:
            datetimes_ranges.append({"date":act, "num":actual_num})
            act = point['datetime']
            if isinstance(act,str):
                try:
                    act = dt.strptime(act, '%Y-%m-%dT%H:%M:%SZ') # zkusi klasicky format
                except:
                    try:
                        act = dt.strptime(act, '%Y-%m-%dT%H:%MZ') + tdel(0,30)# zkusi jiny vraceny format
                    except: #jinak pouzijeme minulej cas
                        act = datetimes_ranges[-1]["date"]
            actual_num =0
        else:
            actual_num +=1
    datetimes_ranges.append({"date": act, "num": actual_num})
    datetimes_ranges.append({"date":end, "num":0})
    ldr = len(datetimes_ranges)
    datetimes = []
    timestamps = []
    for i in range(0, ldr):
        if i+1 < ldr:
            diff = datetimes_ranges[i+1]["date"] - datetimes_ranges[i]["date"]
            new_date = datetimes_ranges[i]["date"]
            datetimes.append(new_date.strftime("%Y-%m-%dT%H:%M:%S"))
            timestamps.append(int(dt.timestamp(new_date)))
            if datetimes_ranges[i]["num"] > 0:
                adding = diff / datetimes_ranges[i]["num"]
                for j in range(0, datetimes_ranges[i]["num"]):
                    new_date += adding
                    datetimes.append(new_date.strftime("%Y-%m-%dT%H:%M:%S"))
                    timestamps.append(int(dt.timestamp(new_date)))
        else:
            new_date = datetimes_ranges[i]["date"]
            datetimes.append(new_date.strftime("%Y-%m-%dT%H:%M:%S"))
            timestamps.append(int(dt.timestamp(new_date)))
    print(num)
    print(len(timestamps))
    return datetimes, timestamps

def getPropertiesDrive(drive, act_id, car_id, num_of_coords):
    '''
    creates properties dictionary for action drive
    :param drive: {} - action drive from planner output
    :param act_id: int - action id
    :param car_id: int - car id
    :param num_of_coords: int - length of timestamp list
    :return: {"action":"MOVING", "car_id":int, "duration":int, "time":[datetimes], "timestamp":[], "consumption":float
    "datetimes":[datetimes], "datetimes_timestamps":[]} - properties specified for visualizer input
    '''
    properties = {}
    properties["action"] = "MOVING"
    properties["car_id"] = car_id
    properties["duration"] = float(drive['duration'])
    try:
        date_objF = dt.strptime(drive['from'], '%Y-%m-%dT%H:%M:%SZ')  # zkusi klasicky format
    except:
        try:
            date_objF = dt.strptime(drive['from'], '%Y-%m-%dT%H:%MZ')  # zkusi jiny vraceny format
        except:
            date_objF = dt.strptime('0000-1-1T00:00', '%Y-%m-%dT%H:%MZ')

    #date_objF = dt.strptime(drive['from'], '%Y-%m-%dT%H:%M:%SZ')
    date_objT = date_objF + tdel(seconds=drive['duration'])
    properties["time"] = [date_objF.strftime("%Y-%m-%dT%H:%M:%S"), date_objT.strftime("%Y-%m-%dT%H:%M:%S")]
    properties["timestamp"] = [int(dt.timestamp(date_objF)), int(dt.timestamp(date_objT))]
    properties['consumption'] = drive['planAction']['consumedEnergy']
    datetimes, datetimes_timestamps = getDatetimesAndTimestamps(date_objF, date_objT, num_of_coords, drive['planAction']['locations'])
    properties['datetimes'] = datetimes
    properties['datetimes_timestamps'] = datetimes_timestamps
    return properties

def oneFeature(geom, prop):
    '''
    creates one feature as specified for visualizer input
    :param geom: {} - geometry
    :param prop: {} - properties
    :return: {"type":Feature,"properties":prop,"geometry":geom}
    '''
    feature = {}
    feature["type"] = "Feature"
    feature["properties"] = prop
    feature["geometry"] = geom
    return feature

def getGeometryCharging(activity):
    '''
    creates geometry dictionary for feature "charge"
    :param activity: {} - action "charge" as in planner output
    :return: {"coordinates":[float, float, float], "type":Point} - geometry for visiualizer input of activity - coordinates
    consists of longitude, latitude, 440.0 in that order
    '''
    geometry = {}
    pom = [activity['planAction']['location']['longitude'], activity['planAction']['location']['latitude'], 440.0]
    geometry['coordinates'] = pom
    geometry['type'] = "Point"
    return geometry

def getPropertiesCharge(charge, act_id, car_id):
    '''
    creates properties dictionary for feature activity
    :param charge: {} - action charge as in planner otuput
    :param act_id: int - action id
    :param car_id: int - car id
    :return:
    '''
    properties = {}
    properties["action"] = "CHARGING"
    properties["car_id"] = car_id
    properties["duration"] = float(charge['duration'])
    try:
        date_objF = dt.strptime(charge['planAction']['time']['from'], '%Y-%m-%dT%H:%M:%SZ')  # zkusi klasicky format
    except:
        try:
            date_objF = dt.strptime(charge['planAction']['time']['from'], '%Y-%m-%dT%H:%MZ')  # zkusi jiny vraceny format
        except:
            date_objF = dt.strptime('0000-1-1T00:00', '%Y-%m-%dT%H:%MZ')
    try:
        date_objT = dt.strptime(charge['planAction']['time']['to'], '%Y-%m-%dT%H:%M:%SZ')  # zkusi klasicky format
    except:
        try:
            date_objT = dt.strptime(charge['planAction']['time']['to'], '%Y-%m-%dT%H:%MZ')  # zkusi jiny vraceny format
        except:
            date_objT = dt.strptime('0000-1-1T00:00', '%Y-%m-%dT%H:%MZ')
    #date_objF = dt.strptime(charge['planAction']['time']['from'], '%Y-%m-%dT%H:%M:%SZ')
    #date_objT = dt.strptime(charge['planAction']['time']['to'], '%Y-%m-%dT%H:%M:%SZ')
    properties["time"] = [date_objF.strftime("%Y-%m-%dT%H:%M:%S"), date_objT.strftime("%Y-%m-%dT%H:%M:%S")]
    properties["timestamp"] = [dt.timestamp(date_objF), dt.timestamp(date_objT)]
    properties['price'] = charge['planAction']['price']
    properties['charging_rate'] = charge['planAction']['chargingRate']
    properties['charged_energy'] = charge['planAction']['chargedEnergy'] #tohle pores az po uprave planovace
    properties['cs_id'] = charge['planAction']['csId']
    return properties

def getGeometryActivity(activity):
    '''
    creates geometry dictionary for feature "activity"
    :param activity: {} - activity as in planner output
    :return: {"coordinates":[float, float, float], "type":Point} - geometry for visiualizer input of activity - coordinates
    consists of longitude, latitude, 440.0 in that order
    '''
    geometry = {}
    pom = [activity['planAction']['location']['longitude'], activity['planAction']['location']['latitude'], 440.0]
    geometry['coordinates'] = pom
    geometry['type'] = "Point"
    return geometry

def getPropertiesActivity(activity, act_id, car_id, activity_name):
    '''
    create dictionary "properties" as specified for visualizer input
    :param activity: {} - activity as in planner output
    :param act_id: int - id of activity
    :param car_id: int - id of car
    :return: {"action":ACTIVITY, "activity_id":int, "car_id":int, "duration":int, time:[datetimes],
    "timestamp":[], "name_of_activity":str, "delay":int, "original_arrival_time":datetime} - as specified for visualizer input
    '''
    properties = {}
    properties["action"] = "ACTIVITY"
    properties["activity_id"] = act_id
    properties["car_id"] = car_id
    properties["duration"] = activity['duration']
    try:
        date_objF = dt.strptime(activity['planAction']['time']['from'], '%Y-%m-%dT%H:%M:%SZ')  # zkusi klasicky format
    except:
        try:
            date_objF = dt.strptime(activity['planAction']['time']['from'], '%Y-%m-%dT%H:%MZ')  # zkusi jiny vraceny format
        except:
            try:
                date_objF = dt.strptime(activity['planAction']['time']['from'], '%Y-%m-%dT%HZ')  # zkusi jiny vraceny format
            except:
                date_objF = dt.strptime('0000-1-1T00:00', '%Y-%m-%dT%H:%MZ')
    try:
        date_objT = dt.strptime(activity['planAction']['time']['to'], '%Y-%m-%dT%H:%M:%SZ')  # zkusi klasicky format
    except:
        try:
            date_objT = dt.strptime(activity['planAction']['time']['to'], '%Y-%m-%dT%H:%MZ')  # zkusi jiny vraceny format
        except:
            try:
                date_objT = dt.strptime(activity['planAction']['time']['to'], '%Y-%m-%dT%HZ')  # zkusi jiny vraceny format
            except:
                date_objT = dt.strptime('0000-1-1T00:00', '%Y-%m-%dT%H:%MZ')
    #date_objF = dt.strptime(activity['planAction']['time']['from'], '%Y-%m-%dT%H:%M:%SZ')
    #date_objT = dt.strptime(activity['planAction']['time']['to'], '%Y-%m-%dT%H:%M:%SZ')#date_objF + tdel(seconds=activity['duration'])
    properties["time"] = [date_objF.strftime("%Y-%m-%dT%H:%M:%S"), date_objT.strftime("%Y-%m-%dT%H:%M:%S")]
    properties["timestamp"] = [dt.timestamp(date_objF), dt.timestamp(date_objT)]
    properties['name_of_activity'] = activity_name
    properties['delay'] = int(activity['planAction']['delay']/60)
    properties['original_arrival_time'] = date_objF.strftime("%Y-%m-%dT%H:%M:%S")
    return properties

def getFeatureCollection(features):
    '''
    creates feature collection from list of features
    :param features: [] of features
    :return: {'features':[], 'type':"FeatureCollection"} - feature collection
    '''
    fc = {}
    fc['features'] = features
    fc['type'] = "FeatureCollection"
    return fc

def getOldActivity(activity, act_id, car_id, delay, activity_name):
    '''
    Processing activities formated as in planner input, returns feature as specified for visualizer input feature
    collection
    :param activity: activity as specified in planner input
    :param act_id: int - id of activity (this is used mainly for first and last)
    :param car_id: int - id of car
    :param delay: int - delay of activity in planner output
    :return: {"type":Feature,, "properties":{}, geometry:{}} - feature (properties and geometry specified as in visualizer
    input and also in docstrings of functions they are created by
    '''
    geom = {}
    geom['coordinates'] = [activity['Pos'][0]['location']['longitude'], activity['Pos'][0]['location']['latitude'], 440.0]
    geom['type'] = "Point"
    properties = {}
    properties["action"] = "ACTIVITY"
    properties["activity_id"] = act_id
    properties["car_id"] = car_id
    properties["duration"] = 0
    properties['delay'] =  delay
    properties['name_of_activity'] = activity_name
    date_objF = dt.strptime(activity['intervals'][0]['from'], '%Y-%m-%dT%H:%M:%S+00:00')
    print(date_objF)
    date_objT = dt.strptime(activity['intervals'][0]['to'], '%Y-%m-%dT%H:%M:%S+00:00')
    date_objF += tdel(minutes=(delay+0))
    print(delay)
    print(date_objF)
    date_objT += tdel(minutes=(delay+1))
    properties["time"] = [date_objF.strftime("%Y-%m-%dT%H:%M:%S"), date_objT.strftime("%Y-%m-%dT%H:%M:%S")]
    properties["timestamp"] = [dt.timestamp(date_objF), dt.timestamp(date_objT)]
    properties['original_arrival_time'] = date_objF.strftime("%Y-%m-%dT%H:%M:%S")
    return oneFeature(geom, properties)
# je treba poresit casove posunuti? (poradne nejak)

def actionsToFeatureCollection(actions, start_pos, end_pos, total_delay, activites_names):
    '''
    From actions list from planner output + start and end actions from (planner )input creates feature collection as
    specified for visualizer input
    :param actions: list of actions from planner output
    :param start_pos: starting action from planner input (as it is not returned in output)
    :param end_pos: ending action from planner input (as it is not returned in output)
    :param total_delay: int - total delay of arrival to final location
    :return: {'features':[], 'type':"FeatureCollection"} - feature collection
    '''
    features = []
    act_id = 0
    features.append(getOldActivity(start_pos, act_id,0,0, "Start: "+ activites_names[act_id]))
    act_id +=1
    for act in actions:
        if act['planAction']['type'] == 'CHARGE':
            geom = getGeometryCharging(act)
            prop = getPropertiesCharge(act, act_id, 0)
            feat = oneFeature(geom, prop)
            features.append(feat)
        elif act['planAction']['type'] == 'DO_ACTIVITY':
            geom = getGeometryActivity(act)
            prop = getPropertiesActivity(act, act_id, 0, activites_names[act_id])
            feat = oneFeature(geom, prop)
            features.append(feat)
            act_id += 1
        elif act['planAction']['type'] == 'DRIVE':
            geom = getGeometryDrive(act)
            prop = getPropertiesDrive(act, act_id, 0, len(geom['coordinates']))
            feat = oneFeature(geom, prop)
            features.append(feat)
    features.append(getOldActivity(end_pos, act_id, 0,int(total_delay/60), "End: "+ activites_names[act_id])) # vyres delay posledni aktivity
    fc = getFeatureCollection(features)
    return fc

def saveGEOJSON(jsondict, name):
    '''
    saving dict as json json
    :param jsondict: {}
    :param name: str
    :return: --
    '''
    with open(name, 'w') as fp:
        json.dump(jsondict, fp)