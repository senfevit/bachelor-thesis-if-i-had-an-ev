import requests
import json
import visualizer_output_processing as vop
import response_processing as rp
import visualiser_preprocessing as vp
import comparison_creation as cc
import kml_processing as kp

def accessLocalPlannerWithFile(input_file_name, ouput_file_name):
    '''
    Created for easier accessing of the planner
    :param input_file_name: str - name of input file
    :param ouput_file_name: str - name of output file
    :return:
    '''
    f = open(input_file_name)
    data = json.load(f)

    response = requests.post('http://localhost:8080/adas-service/user/0/optimal-trips-all', json=data)
    with open(ouput_file_name, 'w') as fp:
        json.dump(response.json(), fp)
    return json

def accessLocalPlanner(data):
    '''
    Created for easier accessing of the planner
    :param data
    :return:
    '''

    response = requests.post('http://localhost:8080/adas-service/user/0/optimal-trips-all', json=data)
    return response.json()

def accessPlanner(data):
    '''
    Created for easier accessing of the planner
    :param data
    :return:
    '''

    response = requests.post('http://its.fel.cvut.cz:8082/adas-service/user/0/optimal-trips-all', json=data)
    # print(response)
    return response.json()

def tryToFindSolutionVisualizerOutput(data, delays):
    tst_input = vop.geojsonToJSON(data, delays)
    o = accessPlanner(tst_input)
    if 'code' in o:
        print('Planner error')
        return 'Err', 'Err', 'Err'
    elif (o['plans']['FASTESTTRIP']['attributes']['duration'] == None):
        print('failure with max delay {}'.format(delays['departure_delay']))
        return None, None, None
    else:
        act_n, act = vop.extractInfo(data)
        a = rp.getActionsFromDict(o)
        total_delay = rp.getEndDelay(act, o)
        fc = vp.actionsToFeatureCollection(a, act[0], act[-1], total_delay, act_n)
        icev_input = vop.geojsonToJSON(data, delays, {'range': 99999, 'battery_capacity': 999})
        o_icev = accessPlanner(icev_input)
        a_icev = rp.getActionsFromDict(o_icev)
        total_delay_icev = rp.getEndDelay(act, o_icev)
        fc_icev = vp.actionsToFeatureCollection(a_icev, act[0], act[-1], total_delay_icev, act_n)
        comparison = cc.createComparison(o['plans']['FASTESTTRIP']['actions'], o_icev['plans']['FASTESTTRIP']['actions'],  act, )
    return fc, fc_icev, comparison

def tryToFindSolutionKML(data, delays):
    act_names, activities, tst_input = kp.kmlToJSON(data, {'range':500, 'battery_capacity':55}, delays)
    o = accessPlanner(tst_input)
    if 'code' in o:
        print('Planner error')
        return 'Err', 'Err', 'Err'
    elif (o['plans']['FASTESTTRIP']['attributes']['duration'] == None):
        print('failure with max delay {}'.format(delays['departure_delay']))
        return None, None, None
    else:
        act = kp.activitiesKMLlistToactivitesTimesList(activities)
        a = rp.getActionsFromDict(o)
        total_delay = rp.getEndDelay(act, o)
        fc = vp.actionsToFeatureCollection(a, act[0], act[-1], total_delay, act_names)
        _, _,icev_input = kp.kmlToJSON(data, {'range': 99999, 'battery_capacity': 999}, delays)
        o_icev = accessPlanner(icev_input)
        a_icev = rp.getActionsFromDict(o_icev)
        total_delay_icev = rp.getEndDelay(act, o_icev)
        fc_icev = vp.actionsToFeatureCollection(a_icev, act[0], act[-1], total_delay_icev, act_names)
        comparison = cc.createComparison(o['plans']['FASTESTTRIP']['actions'], o_icev['plans']['FASTESTTRIP']['actions'],  act, )
    return fc, fc_icev, comparison

