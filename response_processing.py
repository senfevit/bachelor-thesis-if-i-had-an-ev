import json
import datetime
import pandas as pd
import geopandas as gpd
from shapely import wkt
from shapely.geometry import LineString

def getActionsFromJSON(json_name):
    '''
    Reads return file of the planner and returns list of all actions
    :param json_name: str - name of output file of planner
    :return: [] with all planned actions
    '''
    resjson = open(json_name, 'r').read()
    resdict = json.loads(resjson)
    resdict = resdict['plans']
    resdict = resdict['FASTESTTRIP']  # tohle snad po uprave planovace bude navic
    actions = resdict['actions']
    return  actions

def filterNonDrivingActions(actions):
    '''
    Take list of all actions and returns it without driving actions
    :param actions: [] of all actions (for example return of getActionsFromJSON)
    :return: [] of actions (filtered - nondriving), int - duration of plan, [] of driving actions
    '''
    nondrivingactions = []
    drivings = []
    total_duration = 0
    for act in actions:
        if act['planAction']['type'] != 'DRIVE':
            nondrivingactions.append(act)
        else:
            drivings.append(act)
        total_duration += act['duration']
    return nondrivingactions, total_duration, drivings

def filterNonChargingActions(actions):
    '''
    Take list of actions a returns it without chargings
    :param actions: [] of actions (for example return of getActionsFromJSON)
    :return: [] of actions (filtered - non charging), [] of chargings
    '''
    nonchargingactions = []
    charging_actions = []
    for act in actions:
        if act['planAction']['type'] != 'CHARGE':
            nonchargingactions.append(act)
        else:
            charging_actions.append(act)
    return nonchargingactions, charging_actions

def getPriceAndDurationOfCharging(chargings):
    '''
    Get sum of prices and durations of charging in plan returned by planner
    :param chargings: list of actions containing only charging
    :return: int - sum of prices of chargings (planner estimation), int - sum of durations of chargings
    '''
    price = 0.0
    duration = 0.0
    for charging in chargings:
        price += charging['planAction']['price']
        duration += charging['duration']
    return price, duration

def getLineString(drives):
    '''
    Takes drives and returns geodataframe containing linestring of them suitable for visualizing
    :param drives: list of actions (only drives)
    :return: geodataframe with linestring
    '''
    linestrings = []
    for drive in drives:
        df = pd.DataFrame(drive['planAction']['locations'])
        geometry = [xy for xy in zip(df.longitude, df.latitude)]
        linestring = LineString(geometry).wkt
        linestring = wkt.loads(linestring)
        # print(linestring.length)
        linestrings.append(linestring)
    df = pd.DataFrame(
        {'geometry': linestrings})
    #df['geometry'] = df['geometry'].apply(wkt.loads)
    gdf = gpd.GeoDataFrame(df, geometry='geometry')
    gdf.set_crs(epsg=4326, inplace=True)
    gdf.to_crs(epsg=3035, inplace=True)
    #gdf.set_crs(epsg=4326, inplace=True)
    #gdf = gpd.GeoDataFrame(geometry=gpd.points_from_xy(df['longitude'], df['latitude']))
    # print(gdf)
    return gdf

def getActionsFromDict(resdict):
    resdict = resdict['plans']
    resdict = resdict['FASTESTTRIP']  # tohle snad po uprave planovace bude navic
    actions = resdict['actions']
    return  actions

def getEndDelay(act, o):  #act = original activities, o = planner output
    try:
        new_time = datetime.datetime.strptime(o['plans']['FASTESTTRIP']['actions'][-1]['from'], '%Y-%m-%dT%H:%M:%SZ')  # zkusi klasicky format
    except:
        try:
            new_time = datetime.datetime.strptime(o['plans']['FASTESTTRIP']['actions'][-1]['from'],'%Y-%m-%dT%H:%MZ')
        except:
            try:
                new_time = datetime.datetime.strptime(o['plans']['FASTESTTRIP']['actions'][-1]['from'],'%Y-%m-%dT%HZ')
            except:
                new_time = datetime.datetime.strptime('0000-01-01T00:00', '%Y-%m-%dT%H:%MZ')
    old_time = datetime.datetime.strptime(act[-1]['intervals'][0]['from'], "%Y-%m-%dT%H:%M:%S+00:00")

    new_time = new_time + datetime.timedelta(seconds=o['plans']['FASTESTTRIP']['actions'][-1]['duration'])
    if new_time > old_time:
        total_delay = (new_time - old_time).seconds
    else:
        total_delay = 0
    return total_delay