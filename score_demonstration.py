def calculateTotalScore(time, delay, price, green):
    total = time + delay + price + green
    total = min(total, 100)
    total = max(total, 0)
    return total

def getScore(bev, icev, per100cons, perliterpol, perkwhpol, kwh_price, liter_price, values):
    suma = 0 
    for k in values.keys():
        suma += values[k]
    if suma != 100:
        print('sum of scores != 100, scores are reweighted')
        for k in values.keys():
            values[k] = (values[k]/suma) * 100
    timescore = getTimeScore(bev, icev, values['time'])
    delayscore = getDelayScore(bev, icev, values['delay'])
    delayscore = max(delayscore, (-2 / 3) * values['delay'])
    pricescore = getFuelScore(bev, icev, liter_price, kwh_price, per100cons, values['price'])
    greenscore = getGreenScore(bev, icev, perliterpol, perkwhpol, per100cons, values['green'])
    return calculateTotalScore(timescore, delayscore, pricescore, greenscore), {'time':timescore, 'delay':delayscore, 'price':pricescore, 'green':greenscore}

def calcTotalTime(actions):
    time = 0
    for a in actions:
        time += a['duration']
    return time

def getTimeScore(bev, icev, value):
    evt = calcTotalTime(bev)
    icet = calcTotalTime(icev)
    return value * (1-((evt- icet)/ icet))

def getTimeInfo(actions):
    delays = []
    durations = []
    dur = 0
    for a in actions:
        if a['type'] == 'ACTIVITY':
            delays.append(a['delay'])
            durations.append(dur)
            dur = 0
        else:
            dur += a['duration']
    return  delays, durations

def getDelayScore(bev, icev, value):
    bevdels, bevdur = getTimeInfo(bev)
    icedel, icedur = getTimeInfo(icev)
    pom = 0
    for i in range(0, len(icedel)):
        pm = bevdels[i]-icedel[i]
        pm = max(0, pm)
        delay = min(bevdels[i], pm)
        pom += (delay**2) / (4*bevdur[i])
    pom /= len(bevdur)
    return value * (1 - pom)

def getFuelScore(bev, icev, liter_price, kwh_price, consumptionper100, value):
    kwh = 0
    for a in bev:
        if a['type'] == 'DRIVE':
            kwh += a['kwh']
    kms = 0
    for a in icev:
        if a['type'] == 'DRIVE':
            kms += a['km']
    cv_price = (kms / 100) * consumptionper100 * liter_price
    ev_price = kwh * kwh_price
    return value * (1 - (ev_price/cv_price))

def getGreenScore(bev, icev, liter_pol, kwh_pol, consumptionper100, value):
    kwh = 0
    for a in bev:
        if a['type'] == 'DRIVE':
            kwh += a['kwh']
    kms = 0
    for a in icev:
        if a['type'] == 'DRIVE':
            kms += a['km']
    cv_price = (kms / 100) * consumptionper100 * liter_pol
    ev_price = kwh * kwh_pol
    return value * (1 - (ev_price/cv_price))


# bev_actions = [{'type':'DRIVE', 'duration':50, 'kwh':10, 'km':80},
#                {'type':'ACTIVITY', 'delay':5, 'duration':5},
#                {'type':'DRIVE', 'duration':50, 'kwh':10, 'km':80},
#                {'type':'ACTIVITY', 'delay':10, 'duration':5}
#                ]
# icev_actions = [{'type':'DRIVE', 'duration':45, 'km':80},
#                {'type':'ACTIVITY', 'delay':0, 'duration':5},
#                {'type':'DRIVE', 'duration':50, 'km':80},
#                {'type':'ACTIVITY', 'delay':5, 'duration':5}
#                ]
